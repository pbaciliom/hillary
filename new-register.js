var express     = require('express'),
    _           = require('lodash'),
    config      = require('./config'),
    bodyParser  = require('body-parser'),
    requestjson = require('request-json'),
    path        = require('path');

var app = module.exports = express.Router();

//-------Nuevo registro
app.post('/new-user', function(req, res) {

//obtengo datos para comparar
  var userScheme = getDataScheme(req);

//preparamos parametros para ir por user y id
  var USERQ = "q={'username':'usuario'}";
  const USER_NAME =  req.body.username;
  var query=USERQ.replace('usuario',USER_NAME);
  var user_idb;
//voy a Mongo por el username a ver si existe
  getUserCollection(req,query).then( function (users){

    if (_.find(users, userScheme.userSearch))
    {
        //El usuario está tomado y no se puede crear
        console.log("entre a no válido ya que el usuario está siendo utilizado");
        return res.status(400).send("El username ya está tomado por favor intente con otro");
    }
    else
    {
        //Obtengo el último id para sumarle 1 y crear el nuevo usuario
        query = "s={'user_id':-1}&l=1&f={'user_id': 1}";

        getUserCollection(req,query).then( function (users)
        {
        console.log ("--------------------------2");
        console.log (users);
        console.log ("--------------------------3");

        user_idb = users[0].user_id;
        user_idb = parseInt(user_idb) + 1;
        console.log (user_idb);

        delete req.body.passwordc;
        req.body.user_id = user_idb;
        //Llamo promesa de post usuarios


        postUserCollection(req).then(function (resf)
            {

        postCuentaMovimientos(req).then (function (resf)
            {
                    console.log("Alta en movimiento cuentas")
                    console.log ("------------------------8---");
                    res.status(201).send(
                        {
                        id_token: req.body.idcliente

                        });

            })




            })
        })

    } //else
})
});
///////  Area de Funciones

/////// Funcion que formatea datos obtenidos desde el front
function getDataScheme(req) {

  var username;
  var type;
  var userSearch = {};

  if(req.body.username) {
    username = req.body.username;
    type = 'username';
    userSearch = { username: username };
  }

  return {
    username: username,
    type: type,
    userSearch: userSearch
  }
}

/////Funcion generica de consulta coleccion de usuarios
function getUserCollection(req,query) {
    console.log("Antes de conectarme a mongo ");

    const API_URL = 'https://api.mlab.com/api/1/databases/proyectohlrpbmhlr/collections/userslogin?';

    return new Promise ( function (resolve,reject)
    {
   //Me conecto a Mongo para traer usuario

   //var urlusersMlab = API_URL+QUERY.replace('usuario',USER_NAME)+config.api_key;
   var urlusersMlab = API_URL+query+config.api_key;
   var clienteMLab = requestjson.createClient(urlusersMlab);

   clienteMLab.get('',function(err,resM,body)
      {
      if (err) {
          console.log("entre a error de Mlab");
          console.log(err);
          reject;
      }
        console.log("entre a ok MLAB regreso mongo");
        console.log("Pinto Body de ");
        console.log(urlusersMlab);
        console.log(body);
        console.log("------------------1");
        resolve(body);
        })
})
}

/////Funcion de insercion de usuarios

function postUserCollection(req) {
    console.log("Antes de conectarme a mongo post promise");

    //mongo bueno
    var urlmovimientosMlab = 'https://api.mlab.com/api/1/databases/proyectohlrpbmhlr/collections/userslogin?'+config.api_key;
    // mongo prueba
//    var urlmovimientosMlab = 'https://api.mlab.com/api/1/databases/bdbanca2hlr/collections/movimientos?apiKey=ZCfAhJuS_-bVqga6dGGP7TmKaRVjxH32';
    var clienteMLab = requestjson.createClient(urlmovimientosMlab);

    app.use(bodyParser.json());
    return new Promise ( function (resolve,reject)
    {
    ////-----------
    clienteMLab.post('',req.body,function(err,resM,body)
    {
        console.log ("---------------Post a User collection ");
      if(err)
      {
        console.log(body);
        reject;
      }
      else
      {
        console.log("Body de ok de post user collection");
        console.log ("--------------------------5");
        console.log(body);
        resolve(body);

      }
    })
    });
}



/////Funcion de insercion de usuarios

function postCuentaMovimientos(req) {

    //mongo bueno
    var urlmovimientosMlab = 'https://api.mlab.com/api/1/databases/proyectohlrpbmhlr/collections/CuentaMovimientos?'+config.api_key;
    // mongo prueba
//    var urlmovimientosMlab = 'https://api.mlab.com/api/1/databases/bdbanca2hlr/collections/movimientos?apiKey=ZCfAhJuS_-bVqga6dGGP7TmKaRVjxH32';
    var clienteMLab = requestjson.createClient(urlmovimientosMlab);
    delete req.body.password;
    delete req.body.username;
    req.body.idcliente = req.body.user_id;
    delete req.body.user_id;
    delete req.body.name;
    delete req.body.surname;
    delete req.body.surname2;
    delete req.body.email;

    app.use(bodyParser.json());

    return new Promise ( function (resolve,reject)
    {
    ////-----------
    clienteMLab.post('',req.body,function(err,resM,body)
    {
        console.log ("-----------------Post Cuenta Movimientos  7 ");
      if(err)
      {
        console.log(body);
        reject;
      }
      else
      {
        console.log("Body de ok de post");
        console.log ("----------------------Ok Post Cuenta 8");
        console.log(body);
        resolve(body);

      }

        })
    })
}
