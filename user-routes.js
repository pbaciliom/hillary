var express = require('express'),
    _       = require('lodash'),
    config  = require('./config'),
    jwt     = require('jsonwebtoken');

var app = module.exports = express.Router();



function createIdToken(user) {
  return jwt.sign(_.omit(user, 'password'), config.secret, { expiresIn: 60*60*5 });
}

function createAccessToken() {
  return jwt.sign({
    iss: config.issuer,
    aud: config.audience,
    exp: Math.floor(Date.now() / 1000) + (60 * 60),
    scope: 'full_access',
    sub: "lalala|pbmhlr",
    jti: genJti(), // unique identifier for the token
    alg: 'HS256'
  }, config.secret);
}

// Generate Unique Identifier for the access token
function genJti() {
  let jti = '';
  let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (let i = 0; i < 16; i++) {
      jti += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return jti;
}


function getUserScheme(req) {

  var username;
  var type;
  var userSearch = {};

  //  username
  if(req.body.username) {
    username = req.body.username;
    type = 'username';
    userSearch = { username: username };
  }

  return {
    username: username,
    type: type,
    userSearch: userSearch
  }
}

function getUserLogin(req) {
    console.log("Antes de conectarme a mongo ");

    const API_URL = 'https://api.mlab.com/api/1/databases/proyectohlrpbmhlr/collections/userslogin?';
    const QUERY = "q={'username':'usuario'}";
    const USER_NAME =  req.body.username;

    return new Promise ( function (resolve,reject)
    {
   //Me conecto a Mongo para traer usuario
   var requestjson= require('request-json');
   var path = require('path');
   var urlusersMlab = API_URL+QUERY.replace('usuario',USER_NAME)+config.api_key;

   console.log(urlusersMlab);

   var clienteMLab = requestjson.createClient(urlusersMlab);

   clienteMLab.get('',function(err,resM,body)
      {
      if (err) {
          console.log("entre a error de Mlab");
          console.log(err);
          reject;
      }
        console.log("entre a ok MLAB regreso mongo");
        console.log(body);
        resolve(body);
        })
})
}
//------Manejo de login
app.post('/sessions/create', function(req, res)
{
//userScheme trae el contexto del front, usuario y password
  var userScheme = getUserScheme(req);
  console.log(req.body);
  if (!userScheme.username || !req.body.password)
  {
        console.log("Error 1 no puso cualquiera de los 2 ");
    return res.status(400).send("Debe de indicar usuario y contraseña");
  }
  ///var users;  Como está en la function ya no se requiere declarar
  getUserLogin(req).then( function (users){

  user = _.find(users, userScheme.userSearch);

  if (!user) {
     console.log("entre a no valido no existe usuario");
     return res.status(401).send("El usuario o el password no coinciden");
            }
if (user.password !== req.body.password) {
    console.log("entre a no valido no existe password malo");
    return res.status(401).send("El usuario o el password no coinciden");
}
res.status(201).send(
    {
        id_token: createIdToken(user),
        access_token: createAccessToken(),
        id_client: user.user_id,
        name: user.name

    })

})
}
///cierro  llave post
); ///cierro post
