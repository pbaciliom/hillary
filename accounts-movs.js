var express        = require('express'),
    config         = require('./config'),
    bodyParser     = require('body-parser'),
    errorhandler   = require('errorhandler')
    bodyParser     = require('body-parser'),
    requestjson    = require('request-json'),
    jwt            = require('express-jwt'),
    path           = require('path');
    const MongoClient = require('mongodb').MongoClient;
    const assert = require('assert');


var app = module.exports = express.Router();

// Validate access_token
var jwtCheck = jwt({
  secret: config.secret,
  audience: config.audience,
  issuer: config.issuer
});

// Check for scope
function requireScope(scope) {
  return function (req, res, next) {

      console.log(req.user);

    var has_scopes = req.user.scope === scope;
    if (!has_scopes) {
        res.sendStatus(401);
        return;
    }
    next();
  };
}


app.use("/protected", jwtCheck, requireScope('full_access'));
////Obtener detalle de cuentas
app.get("/protected/CuentaMovimientos", function(req, res) {

console.log("Entro a cuenta Movimientos")

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    //console.log ("req.body.id_client")
    //console.log(req.body.id_client);
    console.log ("req.body")
    console.log(req.body);


//preparamos parametros para ir por user y id
  var USERQ = "q={'idcliente':id_cliente}";
  const ID_CLIENT =  req.headers.id_client;
  //const ID_CLIENT = 1235;
  var query=USERQ.replace('id_cliente',ID_CLIENT);
  const API_URL = 'https://api.mlab.com/api/1/databases/proyectohlrpbmhlr/collections/CuentaMovimientos?';
  var urlusersMlab = API_URL+query+config.api_key;
   //console.log(urlusersMlab);

  var clienteMlab = requestjson.createClient(urlusersMlab);

  clienteMlab.get('', function (err, resM, body) {
    if(err) {
      console.log(body);
    }
    else {

      res.send(body);
    }
  })
})
//**************************************************************
//**************************************************************
app.post("/protected/new-movimiento", function(req, res) {
  //preparamos parametros para ir id_cliente

  // Connection URL
  //const url = 'mongodb://admin:admin123@ds125892.mlab.com:25892/bdbanca2hlr';
  const url ='mongodb://admin:admin123@ds131902.mlab.com:31902/proyectohlrpbmhlr';
  // Database Name
  const dbName = 'proyectohlrpbmhlr';
  // Create a new MongoClient
  const client = new MongoClient(url);
  // Use connect method to connect to the Server
  client.connect(
    function(err,response) {
  if(err)
  {
    console.log ("ERROR");
    res.status(400)
  }

        console.log("Connected successfully to server-------------1");
        var db = client.db(dbName);
        postMovsCollection(req,db).then(function (resf,err)  {
        console.log ("De regreso de put---------6");
        client.close();
        if(err){
          console.log ("Despues de cerrar conexion y tratar de hace update");
          console.log (err);
          console.log ("Despues de cerrar conexion y tratar de hace update");
          res.status(400)
        }
          else
              {
                console.log ("Cuenta dada de alta ok !!!_______7");
                res.status(201).send(req.body.id_cuenta);
                }
          }); //cierra post

    }); //cierra connect
}); //cierra post

/////Funcion de insercion de cuentas
function postMovsCollection (req,db){

    console.log("Inicio post que deberia ser put_______2")
  const collection = db.collection('CuentaMovimientos');
// Insert some documents
  var toIdcliente = req.body.idcliente;
  delete req.body.idcliente;
  var saldoToFloat = parseFloat(req.body.saldo)
  req.body.saldo = saldoToFloat;
  console.log(toIdcliente);
  var toInsert = req.body;
  console.log(toInsert);

  return new Promise ( function (resolve,reject)
  {

    console.log("Entro a promesa________3")
    collection.update(
      { idcliente: toIdcliente },
      { $addToSet: { 'cuentas': toInsert } } ,
      function(err, result)
        {
          console.log ("update------------4");
          if(err)
            {
              console.log("entre a error de update");
              reject;
            }
          else
          {
          console.log("Body de ok de update");
          console.log ("--------------------------5");
          //console.log(result);
          resolve(result);
          }
        }); //Cierro update
    });  // Cierro promesa
  }
