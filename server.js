var cors            = require('cors'),
    http            = require('http'),
    express         = require('express'),
    errorhandler    = require('errorhandler'),
    dotenv          = require('dotenv'),
    bodyParser      = require('body-parser'),
    requestjson     = require('request-json');

    app = express(),
    port = process.env.PORT || 3000;

  dotenv.load();
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.use(cors());
  app.use(require('./user-routes'));
  app.use(require('./new-register'));
  app.use(require('./accounts-movs'));
  app.listen(port);

console.log('All ready Hillary RESTful API server started on: ' + port);

app.get("/", function(req, res)
{
  res.sendFile(path.join(__dirname, "index.html"));
})
